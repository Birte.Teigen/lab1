package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true) {
            System.out.println("Let's play round number " + roundCounter);
            String playerMove = playerchoice();

            // use random to chose computermove

            Random random = new Random();
            int randomNumber = random.nextInt(3);

            String computerMove;
            if (randomNumber == 0) {
                computerMove = rpsChoices.get(0);
            } else if (randomNumber == 1) {
                computerMove = rpsChoices.get(1);
            } else {
                computerMove = rpsChoices.get(2);
            }
        
            String outcome;
            if (playerMove.equals(computerMove)) {
                outcome = "It's a draw!";
            } else if (playerWins(playerMove, computerMove)) {
                humanScore++;
                outcome = "Player wins!";
            } else {
                computerScore++;
                outcome = "Computer wins!";
            }
        
            // print

            System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". " + outcome);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            // end game or continue playing

            String continuePlaying = answer();
            if (continuePlaying.equals("n")) {
                break;
            } else {
                roundCounter++;
            }
        }
        
        System.out.println("Bye bye :)");
    }

    private String playerchoice() {
        while (true) {
            String playerinput = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (rpsChoices.contains(playerinput)) {
                return playerinput;
            } else {
                System.out.println("I do not understand " + playerinput + ". Could you try again?");
            }
        }
    }

    private String answer() {
        while (true) {
            String answerInput = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (answerInput.equals("y")) {
                return answerInput;
            } else if (answerInput.equals("n")) {
                return answerInput;
            } else {
                System.out.println("I do not understand " + answerInput + ", try again.");
            }
        }
    }

    static boolean playerWins(String playerMove, String computerMove) {
        if (playerMove.equals("rock")) {
            return computerMove.equals("scissors");
        } else if (playerMove.equals("paper")) {
            return computerMove.equals("rock");
        } else {
            return computerMove.equals("paper");
        }
    }

        // TODO: Implement Rock Paper Scissors
    
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
}
